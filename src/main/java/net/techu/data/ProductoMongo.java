package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("productosdiego")
public class ProductoMongo {

    public String id;
    public String nombre;
    public double precio;
    public String color;

    public ProductoMongo() {
    }

    public ProductoMongo(String nombre, double precio, String color) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, nombre=%s, precio=%s, color=%s]", id, nombre, precio, color);
    }

}
