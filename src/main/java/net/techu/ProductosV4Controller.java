package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller {

    @Autowired
    private ProductoRepositorio repository;

    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Get producto by precio */
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.toString());
        repository.insert(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Optional<ProductoMongo> productoAModificar = repository.findById(id);
            if(productoAModificar.isPresent()) {
                productoAModificar.get().nombre = cambios.nombre;
                productoAModificar.get().precio = cambios.precio;
                productoAModificar.get().color = cambios.color;
            }
            repository.save(productoAModificar.get());
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @DeleteMapping("/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            repository.deleteById(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
}
