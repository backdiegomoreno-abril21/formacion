package net.techu;


import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

//@SpringBootApplication
public class ProductosV3Controller implements CommandLineRunner {

    @Autowired
    private ProductoRepositorio repository;

    public static void main(String[] args) {
        SpringApplication.run(ProductosV3Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando MongoDB");
        repository.findByNombre("prueba");
        //repository.deleteAll();
        //repository.insert(new ProductoMongo("PR1", 99.99, "amarillo"));
        //repository.insert(new ProductoMongo("PR2", 99.99,"azul"));
        //repository.insert(new ProductoMongo("PR2", 99.99, "rojo"));
        List<ProductoMongo> lista = repository.findAll();
        for (ProductoMongo p:lista) {
            System.out.println(p.toString());
        }
    }
}
