package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {

    private ArrayList<String> listaProductos = null;

    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    /* Get lista de productos */
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado() {
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<String> addProducto() {
        listaProductos.add("NEW");
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Get un producto en especifico */
    @GetMapping(value="/productos/{id}")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex) {
            resultado = "No se ha encontrado el producto";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Get lista de productos */
    @PostMapping(value = "/productos/{name}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("name") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoria " + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Modificar producto */
    @PutMapping(value = "/productos/{id}", produces="application/json")
    public ResponseEntity<String> modifyProducto(@PathVariable int id, @RequestBody String body) {

        String resultado = null;
        ResponseEntity<String> respuesta = null;
        System.out.println("Este es el body " + body);
        try {
            resultado = listaProductos.set(id, body);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex) {
            resultado = "Producto no exite";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Borrar producto */
    @DeleteMapping(value = "productos/{id}", produces="application/json")
    public ResponseEntity<String> deleteProducto(@PathVariable int id) {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try {
            resultado = listaProductos.remove(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex) {
            resultado = "Producto no exite";
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

}
