FROM openjdk:15-jdk-alpine
COPY target/formacion-0.0.1-SNAPSHOT.jar api.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/api.jar"]